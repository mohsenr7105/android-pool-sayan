package ir.mimrahe.poolsayan.custom_component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import ir.mimrahe.poolsayan.R;

public class TextViewWithFont extends AppCompatTextView {
    public TextViewWithFont(Context context) {
        super(context);
    }

    public TextViewWithFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TextViewWithFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.WithFont);

//        Log.e("typed array", typedArray.toString());

        String fontAsset = typedArray.getString(R.styleable.WithFont_typefaceAsset);

//        Log.e("font asset", fontAsset);

        if (!TextUtils.isEmpty(fontAsset)){
            Typeface font = Typeface.createFromAsset(context.getAssets(), fontAsset);
            int style = Typeface.NORMAL;

            if (getTypeface() != null)
                style = getTypeface().getStyle();

            setTypeface(font, style);
        }
    }
}
