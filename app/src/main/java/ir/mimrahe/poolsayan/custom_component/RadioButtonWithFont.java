package ir.mimrahe.poolsayan.custom_component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import ir.mimrahe.poolsayan.R;

public class RadioButtonWithFont extends AppCompatRadioButton {
    public RadioButtonWithFont(Context context) {
        super(context);
    }

    public RadioButtonWithFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RadioButtonWithFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.WithFont);

//        Log.e("radio typed array", typedArray.toString());

        String fontAsset = typedArray.getString(R.styleable.WithFont_typefaceAsset);

//        Log.e("radio font asset", fontAsset);

        if (!TextUtils.isEmpty(fontAsset)){
            Typeface font = Typeface.createFromAsset(context.getAssets(), fontAsset);

            setTypeface(font);
        }
    }
}
