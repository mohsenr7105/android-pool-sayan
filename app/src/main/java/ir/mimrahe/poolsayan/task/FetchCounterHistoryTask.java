package ir.mimrahe.poolsayan.task;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

import ir.mimrahe.poolsayan.callback.CounterResponseListener;
import ir.mimrahe.poolsayan.model.CounterHistoryModel;

public class FetchCounterHistoryTask extends AsyncTask<Context, Void, ArrayList<CounterHistoryModel>> {
    private int startIndex;
    private CounterResponseListener responseListener;

    public FetchCounterHistoryTask(int startIndex, CounterResponseListener responseListener){
        this.startIndex = startIndex;
        this.responseListener = responseListener;
    }

    @Override
    protected ArrayList<CounterHistoryModel> doInBackground(Context... contexts) {
        return CounterHistoryModel.findWithLimit(contexts[0], startIndex, 10);
    }

    @Override
    protected void onPostExecute(ArrayList<CounterHistoryModel> histories) {
        responseListener.onResponse(histories);
    }
}
