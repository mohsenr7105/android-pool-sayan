package ir.mimrahe.poolsayan.task;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

import ir.mimrahe.poolsayan.callback.ConverterResponseListener;
import ir.mimrahe.poolsayan.model.ConverterHistoryModel;

public class FetchConverterHistoryTask extends AsyncTask<Context, Void, ArrayList<ConverterHistoryModel>> {
    private int startIndex;
    private ConverterResponseListener responseListener;

    public FetchConverterHistoryTask(int startIndex, ConverterResponseListener responseListener){
        this.startIndex = startIndex;
        this.responseListener = responseListener;
    }

    @Override
    protected ArrayList<ConverterHistoryModel> doInBackground(Context... contexts) {
        return ConverterHistoryModel.findWithLimit(contexts[0], startIndex, 10);
    }

    @Override
    protected void onPostExecute(ArrayList<ConverterHistoryModel> histories) {
        responseListener.onResponse(histories);
    }
}
