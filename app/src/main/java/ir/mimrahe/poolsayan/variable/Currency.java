package ir.mimrahe.poolsayan.variable;

public enum Currency {
    Rial("rial"), Tooman("tooman");

    private String value;

    Currency(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
