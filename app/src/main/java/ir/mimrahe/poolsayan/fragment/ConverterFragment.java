package ir.mimrahe.poolsayan.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import java.util.Date;

import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.activity.HistoryActivity;
import ir.mimrahe.poolsayan.custom_component.EditTextWithFont;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.model.ConverterHistoryModel;
import ir.mimrahe.poolsayan.tool.Alerts;
import ir.mimrahe.poolsayan.tool.InWords;
import ir.mimrahe.poolsayan.tool.MiniHelpers;
import ir.mimrahe.poolsayan.variable.Currency;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConverterFragment extends Fragment implements Alerts.AlertCallback {
    EditTextWithFont editTextFrom;
    RadioGroup radioGroupCurrency;
    TextViewWithFont textViewInRialInNumber, textViewInRialInWord,
            textViewInToomanInNumber, textViewInToomanInWord;

    ConverterFragmentCallback converterFragmentCallback;

    private static final String TAG_LOG = "converter fragment";

    public ConverterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        log("on create view");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_converter, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        log("on activity created");
        setViews();
        converterFragmentCallback = (ConverterFragmentCallback) getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log("on create");
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        log("on start");
        converterFragmentCallback.onFragmentStart(R.string.converter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        log("on destroy");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.converter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_clear:
                clearField();
                break;
            case R.id.menu_item_save:
                if (editTextFrom.getText().toString().isEmpty() || Long.valueOf(editTextFrom.getText().toString()) == 0)
                    break;
                Alerts.showSave(getActivity(), this);
                break;
            case R.id.menu_item_history:
                Intent intent = new Intent(getActivity(), HistoryActivity.class);
                intent.putExtra(HistoryActivity.TAG_HISTORY_NAME, HistoryActivity.TAG_CONVERTER_HISTORY);
                startActivity(intent);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private Context getApplicationContext(){
        return getActivity().getApplicationContext();
    }

    private View findViewById(@IdRes int id){
        return getView().findViewById(id);
    }

    private void setViews(){
        editTextFrom = (EditTextWithFont) findViewById(R.id.editTextFrom);

        editTextFrom.addTextChangedListener(new InputTextWatcher());

        radioGroupCurrency = (RadioGroup) findViewById(R.id.radioGroupCurrency);
        radioGroupCurrency.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                calcAndShow(editTextFrom.getText().toString());
            }
        });

        textViewInRialInNumber = (TextViewWithFont) findViewById(R.id.textViewInRialInNumber);
        textViewInRialInWord = (TextViewWithFont) findViewById(R.id.textViewInRialInWord);
        textViewInToomanInNumber = (TextViewWithFont) findViewById(R.id.textViewInToomanInNumber);
        textViewInToomanInWord = (TextViewWithFont) findViewById(R.id.textViewInToomanInWord);
    }

    private Currency getCurrentCurrency(){
        return radioGroupCurrency.getCheckedRadioButtonId() == R.id.radioRial ? Currency.Rial : Currency.Tooman;
    }

    private void calcAndShow(String number){
        number = number.length() == 0 ? "0" : number;
        // define variables
        long numberRial, numberTooman;
        String inWordsRial, inWordsTooman;
        // get bootstrap values
        if (getCurrentCurrency().equals(Currency.Rial)){
            numberRial = Long.valueOf(number);
            numberTooman = numberRial / 10;
        } else {
            numberTooman = Long.valueOf(number);
            numberRial = numberTooman * 10;
        }
        // get in words
        inWordsRial = InWords.from(getApplicationContext(), numberRial);
        inWordsTooman = InWords.from(getApplicationContext(), numberTooman);
        // set text views
        textViewInRialInNumber.setText(MiniHelpers.numberFormat(numberRial));
        textViewInRialInWord.setText(inWordsRial);

        textViewInToomanInNumber.setText(MiniHelpers.numberFormat(numberTooman));
        textViewInToomanInWord.setText(inWordsTooman);
    }

    public void clearField(){
        editTextFrom.setText("");
    }

    private void log(String message){
        Log.e(TAG_LOG, message);
    }

    @Override
    public void onSaveInHistory(String title) {
        ConverterHistoryModel converterHistory = new ConverterHistoryModel(getApplicationContext());

        converterHistory.setCreatedAt(new Date())
        .setTitle(title)
        .setResultInWordsRial(textViewInRialInWord.getText().toString())
        .setResultInWordsTooman(textViewInToomanInWord.getText().toString())
        .setResultNumbersRial(textViewInRialInNumber.getText().toString())
        .setResultNumbersTooman(textViewInToomanInNumber.getText().toString());

        clearField();

        if (converterHistory.save())
            Alerts.toast(getApplicationContext(), R.string.info_saved);
    }

    private class InputTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            calcAndShow(s.toString());
        }
    }

    public interface ConverterFragmentCallback{
        void onFragmentStart(int titleRes);
    }
}
