package ir.mimrahe.poolsayan.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.activity.HistoryActivity;
import ir.mimrahe.poolsayan.custom_component.EditTextWithFont;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.model.CounterHistoryModel;
import ir.mimrahe.poolsayan.tool.Alerts;
import ir.mimrahe.poolsayan.tool.InWords;
import ir.mimrahe.poolsayan.tool.MiniHelpers;
import ir.mimrahe.poolsayan.tool.PrefManager;
import ir.mimrahe.poolsayan.variable.Currency;

/**
 * A simple {@link Fragment} subclass.
 */
public class CounterFragment extends Fragment implements Alerts.CounterAlertCallback, Alerts.AlertCallback, View.OnClickListener {
    EditTextWithFont editText100000, editText50000, editText10000, editText5000, editText2000,
            editText1000, editText500, editText200, editText100, editText50;
    TextViewWithFont textViewResult100000, textViewResult50000, textViewResult10000, textViewResult5000,
            textViewResult2000, textViewResult1000, textViewResult500,
            textViewResult200, textViewResult100, textViewResult50,
            textViewResult, textViewCurrency, textViewChangeCurrency,
            textViewResultInWords,
            textView100000, textView50000, textView10000, textView5000, textView2000, textView1000,
            textView500, textView200, textView100, textView50;
    LinearLayout layoutAdditionalBills;
    HashMap<String, TextViewWithFont> hashMapBills = new HashMap<>();
    HashMap<String, TextViewWithFont> hashMapResults = new HashMap<>();
    HashMap<String, EditTextWithFont> hashMapEdits = new HashMap<>();
    ArrayList<String> arrayListBills = new ArrayList<>();
    ArrayList<String> arrayListAdditionalBills = new ArrayList<>();

    CounterFragmentCallback counterFragmentCallback;

    String status_currency;
    boolean settings_show_additional_bills;

    private static final String TAG_LOG = "counter fragment";
    private Menu mMenu;

    public CounterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        log("on create view");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_counter, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setArrayListBills();
        setArrayListAdditionalBills();
        setViews();
        setHashMapBills();
        setHashMapResults();
        setHashMapEdits();
        setEditTextListeners();

        PrefManager.init(getApplicationContext());
        status_currency = PrefManager.getCurrency();
        setTextViewCurrency();

        settings_show_additional_bills = PrefManager.getShowAdditionalBills();
        showAdditionalBills(settings_show_additional_bills);

        try {
            counterFragmentCallback = (CounterFragmentCallback) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnAdditionalBillsVisibilityChangedListener");
        }

        log("on activity created");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log("on create");
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        log("on start");
        counterFragmentCallback.onFragmentStart(R.string.bill_counter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        log("on destroy");
        PrefManager.destroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        inflater.inflate(R.menu.counter, menu);
        super.onCreateOptionsMenu(menu, inflater);
        changeAdditionalVisibilityIcon(settings_show_additional_bills);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_clear:
                clearFields();
                break;
            case R.id.menu_item_settings:
                Alerts.showSettings(getActivity(), this);
                break;
            case R.id.menu_item_save:
                if (textViewResult.getText().toString().length() <= 1)
                    break;
                Alerts.showSave(getActivity(), this);
                break;
            case R.id.menu_item_history:
                Intent intent = new Intent(getActivity(), HistoryActivity.class);
                intent.putExtra(HistoryActivity.TAG_HISTORY_NAME, HistoryActivity.TAG_COUNTER_HISTORY);
                startActivity(intent);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private Context getApplicationContext(){
        return getActivity().getApplicationContext();
    }

    private View findViewById(@IdRes int id){
        return getView().findViewById(id);
    }

    private void setArrayListBills(){
        arrayListBills.add("100000");
        arrayListBills.add("50000");
        arrayListBills.add("10000");
        arrayListBills.add("5000");
        arrayListBills.add("2000");
        arrayListBills.add("1000");
        arrayListBills.add("500");
    }

    private void setArrayListAdditionalBills(){
        arrayListAdditionalBills.add("200");
        arrayListAdditionalBills.add("100");
        arrayListAdditionalBills.add("50");
    }

    private void setViews(){
        layoutAdditionalBills = (LinearLayout) findViewById(R.id.layout_additional_bills);

        editText50 = (EditTextWithFont) findViewById(R.id.editText50);
        editText100 = (EditTextWithFont) findViewById(R.id.editText100);
        editText200 = (EditTextWithFont) findViewById(R.id.editText200);
        editText500 = (EditTextWithFont) findViewById(R.id.editText500);
        editText1000 = (EditTextWithFont) findViewById(R.id.editText1000);
        editText2000 = (EditTextWithFont) findViewById(R.id.editText2000);
        editText5000 = (EditTextWithFont) findViewById(R.id.editText5000);
        editText10000 = (EditTextWithFont) findViewById(R.id.editText10000);
        editText50000 = (EditTextWithFont) findViewById(R.id.editText50000);
        editText100000 = (EditTextWithFont) findViewById(R.id.editText100000);

        textViewResult50 = (TextViewWithFont) findViewById(R.id.textViewResult50);
        textViewResult100 = (TextViewWithFont) findViewById(R.id.textViewResult100);
        textViewResult200 = (TextViewWithFont) findViewById(R.id.textViewResult200);
        textViewResult500 = (TextViewWithFont) findViewById(R.id.textViewResult500);
        textViewResult1000 = (TextViewWithFont) findViewById(R.id.textViewResult1000);
        textViewResult2000 = (TextViewWithFont) findViewById(R.id.textViewResult2000);
        textViewResult5000 = (TextViewWithFont) findViewById(R.id.textViewResult5000);
        textViewResult10000 = (TextViewWithFont) findViewById(R.id.textViewResult10000);
        textViewResult50000 = (TextViewWithFont) findViewById(R.id.textViewResult50000);
        textViewResult100000 = (TextViewWithFont) findViewById(R.id.textViewResult100000);

        textView50 = (TextViewWithFont) findViewById(R.id.textView50);
        textView100 = (TextViewWithFont) findViewById(R.id.textView100);
        textView200 = (TextViewWithFont) findViewById(R.id.textView200);
        textView500 = (TextViewWithFont) findViewById(R.id.textView500);
        textView1000 = (TextViewWithFont) findViewById(R.id.textView1000);
        textView2000 = (TextViewWithFont) findViewById(R.id.textView2000);
        textView5000 = (TextViewWithFont) findViewById(R.id.textView5000);
        textView10000 = (TextViewWithFont) findViewById(R.id.textView10000);
        textView50000 = (TextViewWithFont) findViewById(R.id.textView50000);
        textView100000 = (TextViewWithFont) findViewById(R.id.textView100000);

        textViewResult = (TextViewWithFont) findViewById(R.id.textViewResult);
        textViewResultInWords = (TextViewWithFont) findViewById(R.id.textViewResultInWords);

        textViewCurrency = (TextViewWithFont) findViewById(R.id.textViewCurrency);

        textViewChangeCurrency = (TextViewWithFont) findViewById(R.id.textViewChangeCurrency);
        textViewChangeCurrency.setOnClickListener(this);
    }

    private void setHashMapBills(){
        hashMapBills.put("50", textView50);
        hashMapBills.put("100", textView100);
        hashMapBills.put("200", textView200);
        hashMapBills.put("500", textView500);
        hashMapBills.put("1000", textView1000);
        hashMapBills.put("2000", textView2000);
        hashMapBills.put("5000", textView5000);
        hashMapBills.put("10000", textView10000);
        hashMapBills.put("50000", textView50000);
        hashMapBills.put("100000", textView100000);
    }

    private void setHashMapResults(){
        hashMapResults.put("50", textViewResult50);
        hashMapResults.put("100", textViewResult100);
        hashMapResults.put("200", textViewResult200);
        hashMapResults.put("500", textViewResult500);
        hashMapResults.put("1000", textViewResult1000);
        hashMapResults.put("2000", textViewResult2000);
        hashMapResults.put("5000", textViewResult5000);
        hashMapResults.put("10000", textViewResult10000);
        hashMapResults.put("50000", textViewResult50000);
        hashMapResults.put("100000", textViewResult100000);
    }

    private void setHashMapEdits(){
        hashMapEdits.put("50", editText50);
        hashMapEdits.put("100", editText100);
        hashMapEdits.put("200", editText200);
        hashMapEdits.put("500", editText500);
        hashMapEdits.put("1000", editText1000);
        hashMapEdits.put("2000", editText2000);
        hashMapEdits.put("5000", editText5000);
        hashMapEdits.put("10000", editText10000);
        hashMapEdits.put("50000", editText50000);
        hashMapEdits.put("100000", editText100000);
    }

    private void setEditTextListeners(){
        editText50.addTextChangedListener(new MyTextWatcher().setBill("50"));
        editText100.addTextChangedListener(new MyTextWatcher().setBill("100"));
        editText200.addTextChangedListener(new MyTextWatcher().setBill("200"));
        editText500.addTextChangedListener(new MyTextWatcher().setBill("500"));
        editText1000.addTextChangedListener(new MyTextWatcher().setBill("1000"));
        editText2000.addTextChangedListener(new MyTextWatcher().setBill("2000"));
        editText5000.addTextChangedListener(new MyTextWatcher().setBill("5000"));
        editText10000.addTextChangedListener(new MyTextWatcher().setBill("10000"));
        editText50000.addTextChangedListener(new MyTextWatcher().setBill("50000"));
        editText100000.addTextChangedListener(new MyTextWatcher().setBill("100000"));
    }

    private void setTextViewCurrency(){
        textViewCurrency.setText(getCurrencyRes());
        changeBillsCurrency();
    }

    private void showAdditionalBills(boolean show){
        layoutAdditionalBills.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void clearFields(){
        for (String bill: arrayListBills){
            hashMapEdits.get(bill).setText("");
        }
        for (String bill: arrayListAdditionalBills){
            hashMapEdits.get(bill).setText("");
        }
    }

    private void changeValues(){
        String count;
        long number, result, sum = 0;
        for (String bill: arrayListBills){
            count = hashMapEdits.get(bill).getText().toString();
            number = count.length() == 0 ? 0 : Long.valueOf(count);
            result = number * Long.valueOf(bill);
            result *= getCurrencyNumber();
            sum += result;
            hashMapResults.get(bill).setText(MiniHelpers.numberFormat(result));
        }
        for (String bill: arrayListAdditionalBills){
            count = hashMapEdits.get(bill).getText().toString();
            number = count.length() == 0 ? 0 : Long.valueOf(count);
            result = number * Long.valueOf(bill);
            result *= getCurrencyNumber();
            sum += result;
            hashMapResults.get(bill).setText(MiniHelpers.numberFormat(result));
        }
        setTextViewResult(sum);
    }

    private void changeBillsCurrency(){
        int multiply = (status_currency.equals(Currency.Tooman.getValue())) ? 1 : 10;
        for (String bill: arrayListBills){
            hashMapBills.get(bill).setText(
                    MiniHelpers.numberFormat(
                            Integer.valueOf(bill) * multiply
                    )
            );
        }
        for (String bill: arrayListAdditionalBills){
            hashMapBills.get(bill).setText(
                    MiniHelpers.numberFormat(
                            Integer.valueOf(bill) * multiply
                    )
            );
        }
    }

    private void sum(){
        long number;
        long result = 0;
        for (String string: arrayListBills){
            String text = hashMapEdits.get(string).getText().toString();
            number = text.length() == 0 ? 0 : Long.valueOf(text);

            result += Long.valueOf(string) * number;
        }
        if(settings_show_additional_bills){
            for (String string: arrayListAdditionalBills){
                String text = hashMapEdits.get(string).getText().toString();
                number = text.length() == 0 ? 0 : Long.valueOf(text);

                result += Long.valueOf(string) * number;
            }
        }
        result *= getCurrencyNumber();
        setTextViewResult(result);
    }

    private void setTextViewResult(long number){
        textViewResult.setText(MiniHelpers.numberFormat(number));
        textViewResultInWords.setText(InWords.from(getApplicationContext(), number) + " " + getCurrencyString());
    }
    
    private int getCurrencyRes(){
        return status_currency.equals(Currency.Rial.getValue()) ? R.string.rial : R.string.tooman;
    }

    private String getCurrencyString(){
        return getString(getCurrencyRes());
    }

    private long getCurrencyNumber(){
        return status_currency.equals(Currency.Rial.getValue()) ? 10 : 1;
    }

    private void changeAdditionalVisibilityIcon(boolean visibility){
        MenuItem additionalBillsVisibility = mMenu.findItem(R.id.menu_item_settings);

        additionalBillsVisibility.setIcon(visibility ? R.drawable.ic_action_visibility_off : R.drawable.ic_visibility_white_24dp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textViewChangeCurrency:
                Alerts.showChangeCurrency(getActivity(), this);
        }
    }

    @Override
    public void onCurrencyChanged(String currency) {
        status_currency = currency;
        PrefManager.setCurrency(currency);
        setTextViewCurrency();
        changeValues();
    }

    @Override
    public void onSettingsChanged(boolean[] settings) {
        // settings: show additional bills
        settings_show_additional_bills = settings[0];
        PrefManager.setShowAdditionalBills(settings_show_additional_bills);
        showAdditionalBills(settings_show_additional_bills);
        changeAdditionalVisibilityIcon(settings_show_additional_bills);
    }

    @Override
    public void onSaveInHistory(String title) {
        CounterHistoryModel counterHistory = new CounterHistoryModel(getApplicationContext());

        JSONObject items = new JSONObject();

        try {
            String count;
            for (String bill: arrayListBills){
                count = hashMapEdits.get(bill).getText().toString().trim();
                if (count.length() < 1)
                    continue;
                if (Integer.valueOf(count).equals(0))
                    continue;
                Log.e(bill, count);
                items.put(bill, Integer.valueOf(count));
            }
            for (String bill: arrayListAdditionalBills){
                count = hashMapEdits.get(bill).getText().toString().trim();
                if (count.length() < 1)
                    continue;
                if (Integer.valueOf(count).equals(0))
                    continue;
                Log.e(bill, count);
                items.put(bill, Integer.valueOf(count));
            }
        } catch (JSONException e){
            Log.e("error in json creating", e.getMessage());
        }

        counterHistory.setCurrency(status_currency)
        .setResultNumbers(textViewResult.getText().toString())
        .setResultInWords(textViewResultInWords.getText().toString())
        .setItems(items)
        .setTitle(title)
        .setCreatedAt(new Date());

        clearFields();

        if (counterHistory.save())
            Alerts.toast(getApplicationContext(), R.string.info_saved);
    }

    private class MyTextWatcher implements TextWatcher {
        private String bill;

        public MyTextWatcher setBill(String bill){
            this.bill = bill;
            return this;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            long number = s.length() == 0 ? 0 : Long.valueOf(s.toString());
            long result = 0;
            if (number != 0){
                result = number * Long.valueOf(bill);
                result *= getCurrencyNumber();
            }

            hashMapResults.get(bill).setText(MiniHelpers.numberFormat(result));

            sum();
        }
    }

    public interface CounterFragmentCallback{
        void onFragmentStart(int titleRes);
    }

    private void log(String message){
        Log.e(TAG_LOG, message);
    }
}
