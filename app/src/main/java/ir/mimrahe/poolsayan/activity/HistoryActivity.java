package ir.mimrahe.poolsayan.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.adapter.ConverterHistoryAdapter;
import ir.mimrahe.poolsayan.adapter.CounterHistoryAdapter;
import ir.mimrahe.poolsayan.callback.ConverterResponseListener;
import ir.mimrahe.poolsayan.callback.CounterResponseListener;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.model.ConverterHistoryModel;
import ir.mimrahe.poolsayan.model.CounterHistoryModel;
import ir.mimrahe.poolsayan.task.FetchConverterHistoryTask;
import ir.mimrahe.poolsayan.task.FetchCounterHistoryTask;
import ir.mimrahe.poolsayan.tool.Alerts;
import ir.mimrahe.poolsayan.tool.FontSpan;
import ir.mimrahe.poolsayan.tool.HistoryLazyLoader;

public class HistoryActivity extends AppCompatActivity implements ListView.MultiChoiceModeListener {
    Toolbar toolbar;
    TextViewWithFont toolbarTitle, textViewMessage;
    ListView listViewHistory;

    String requestedHistory;

    CounterHistoryAdapter counterHistoryAdapter;
    ConverterHistoryAdapter converterHistoryAdapter;

    ArrayList<CounterHistoryModel> counterHistories;
    ArrayList<CounterHistoryModel> counterHistoriesToDelete = new ArrayList<>();
    ArrayList<ConverterHistoryModel> converterHistories;
    ArrayList<ConverterHistoryModel> converterHistoriesToDelete = new ArrayList<>();

    public static final String TAG_HISTORY_NAME = "history_name";
    public static final String TAG_COUNTER_HISTORY = "counter_history";
    public static final String TAG_CONVERTER_HISTORY = "converter_history";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        textViewMessage = (TextViewWithFont) findViewById(R.id.textViewHistoryIsEmpty);
        listViewHistory = (ListView) findViewById(R.id.listViewHistory);
        listViewHistory.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listViewHistory.setMultiChoiceModeListener(this);

        requestedHistory = getIntent().getStringExtra(TAG_HISTORY_NAME);
        switch (requestedHistory){
            case TAG_CONVERTER_HISTORY:
                setToolbar(R.string.converter);
                converterHistories = ConverterHistoryModel.findWithLimit(getApplicationContext(), 0, 10);
                if (converterHistories.isEmpty()){
                    visibleMessageEmpty(true);
                    return;
                }
                converterHistoryAdapter = new ConverterHistoryAdapter(this, converterHistories);
                listViewHistory.setAdapter(converterHistoryAdapter);
                listViewHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Alerts.showConverterHistoryDetails(HistoryActivity.this, converterHistoryAdapter.getItem(position));
                    }
                });
                listViewHistory.setOnScrollListener(converterHistoryLoader);
                break;
            case TAG_COUNTER_HISTORY:
                setToolbar(R.string.bill_counter);
                counterHistories = CounterHistoryModel.findWithLimit(getApplicationContext(), 0, 10);
                if (counterHistories.isEmpty()){
                    visibleMessageEmpty(true);
                    return;
                }
                counterHistoryAdapter = new CounterHistoryAdapter(this, counterHistories);
                listViewHistory.setAdapter(counterHistoryAdapter);
                listViewHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Alerts.showCounterHistoryDetails(HistoryActivity.this, counterHistoryAdapter.getItem(position));
                    }
                });
                listViewHistory.setOnScrollListener(counterHistoryLoader);
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.history_selected ,menu);
    }

    private void setToolbar(int titleResId){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTitle = (TextViewWithFont) toolbar.findViewById(R.id.textViewTitleBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTitle.setText(getString(R.string.history_title, getString(titleResId)));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void visibleMessageEmpty(boolean visible){
        textViewMessage.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
        switch (requestedHistory){
            case TAG_CONVERTER_HISTORY:
                if (checked){
                    converterHistoriesToDelete.add(converterHistoryAdapter.getItem(position));
                } else {
                    converterHistoriesToDelete.remove(converterHistoryAdapter.getItem(position));
                }
                break;
            case TAG_COUNTER_HISTORY:
                if (checked){
                    counterHistoriesToDelete.add(counterHistoryAdapter.getItem(position));
                } else {
                    counterHistoriesToDelete.remove(counterHistoryAdapter.getItem(position));
                }
                break;
        }
        SpannableString s = new SpannableString(getString(R.string.history_selection_title, listViewHistory.getCheckedItemCount()));
        s.setSpan(new FontSpan(this), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mode.setTitle(s);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        getMenuInflater().inflate(R.menu.history_selected, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        int count = 0;

        switch (item.getItemId()){
            case R.id.menu_item_delete:
                switch (requestedHistory){
                    case TAG_CONVERTER_HISTORY:
                        for (ConverterHistoryModel historyItemToDelete: converterHistoriesToDelete){
                            converterHistories.remove(historyItemToDelete);
                            historyItemToDelete.deleteWithID();
                        }
                        count = converterHistoriesToDelete.size();
                        break;
                    case TAG_COUNTER_HISTORY:
                        for (CounterHistoryModel historyItemToDelete: counterHistoriesToDelete) {
                            counterHistories.remove(historyItemToDelete);
                            historyItemToDelete.deleteWithID();
                        }
                        count = counterHistoriesToDelete.size();
                        break;
                }
                if (count > 0)
                    Alerts.toast(this, getString(R.string.message_items_deleted, count));

                mode.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        switch (requestedHistory){
            case TAG_CONVERTER_HISTORY:
                converterHistoriesToDelete.clear();
                visibleMessageEmpty(converterHistories.isEmpty());
                break;
            case TAG_COUNTER_HISTORY:
                counterHistoriesToDelete.clear();
                visibleMessageEmpty(counterHistories.isEmpty());
                break;
        }
    }

    private HistoryLazyLoader counterHistoryLoader = new HistoryLazyLoader() {
        @Override
        public void loadMore() {
            new FetchCounterHistoryTask(counterHistories.size(), counterResponseListener).execute(HistoryActivity.this);
            Log.e("counter history loader", counterHistories.size() + "");
        }
    };

    private CounterResponseListener counterResponseListener = new CounterResponseListener() {
        @Override
        public void onResponse(ArrayList<CounterHistoryModel> responseItems) {
            counterHistories.addAll(responseItems);
            counterHistoryAdapter.notifyDataSetChanged();
            Log.e("on response", "called");
            Log.e("on response", counterHistories.size() + "");
        }
    };

    private HistoryLazyLoader converterHistoryLoader = new HistoryLazyLoader() {
        @Override
        public void loadMore() {
            new FetchConverterHistoryTask(converterHistories.size(), converterResponseListener).execute(HistoryActivity.this);
            Log.e("counter history loader", converterHistories.size() + "");
        }
    };

    private ConverterResponseListener converterResponseListener = new ConverterResponseListener() {
        @Override
        public void onResponse(ArrayList<ConverterHistoryModel> responseItems) {
            converterHistories.addAll(responseItems);
            converterHistoryAdapter.notifyDataSetChanged();
            Log.e("on response", "called");
            Log.e("on response", converterHistories.size() + "");
        }
    };
}
