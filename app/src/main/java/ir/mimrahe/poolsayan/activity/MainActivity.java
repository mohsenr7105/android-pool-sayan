package ir.mimrahe.poolsayan.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import ir.mimrahe.poolsayan.adapter.NavDrawerAdapter;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.fragment.ConverterFragment;
import ir.mimrahe.poolsayan.fragment.CounterFragment;
import ir.mimrahe.poolsayan.model.NavItem;
import ir.mimrahe.poolsayan.tool.Alerts;
import ir.mimrahe.poolsayan.tool.IntentLoader;

public class MainActivity extends AppCompatActivity
        implements CounterFragment.CounterFragmentCallback, ConverterFragment.ConverterFragmentCallback{
    Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    TextViewWithFont toolbarTitle;
    ListView listViewTools, listViewMore;

    private Menu mMenu;

    private final String TAG_FRAG_COUNTER = "fragment_counter";
    private final String TAG_FRAG_CONVERTER = "fragment_converter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbarAndNav();
        setNavMenu();
        placeFragment(TAG_FRAG_COUNTER);
    }

    private void setToolbarAndNav() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle = (TextViewWithFont) toolbar.findViewById(R.id.textViewTitleBar);
    }

    private void setNavMenu() {
        listViewTools = (ListView) findViewById(R.id.listViewTools);
        listViewMore = (ListView) findViewById(R.id.listViewMore);

        NavItem[] tools = new NavItem[]{
                new NavItem(R.string.bill_counter, R.drawable.ic_tune_black_24dp),
                new NavItem(R.string.converter, R.drawable.ic_rotate_90_degrees_ccw_black_24dp)
        };

        NavItem[] more = new NavItem[]{
                new NavItem(R.string.rate_us, R.drawable.ic_star_white_24dp),
                new NavItem(R.string.share, R.drawable.ic_share_black_24dp),
                new NavItem(R.string.contact_developer, R.drawable.ic_chat_black_24dp),
                new NavItem(R.string.see_other_apps, R.drawable.ic_shop_black_24dp),
        };

        listViewTools.setAdapter(new NavDrawerAdapter(this, R.layout.layout_menu_item, tools));
        listViewMore.setAdapter(new NavDrawerAdapter(this, R.layout.layout_menu_item, more));

        listViewTools.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: // bill counter
                        placeFragment(TAG_FRAG_COUNTER);
                        break;
                    case 1: // converter in words
                        placeFragment(TAG_FRAG_CONVERTER);
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        listViewMore.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                drawer.closeDrawer(GravityCompat.START);

                switch (position){
                    case 0:
                        IntentLoader.loadRateUs(MainActivity.this);
                        break;
                    case 1:
                        IntentLoader.loadShareApp(MainActivity.this);
                        break;
                    case 2:
                        IntentLoader.loadContactDev(MainActivity.this);
                        break;
                    case 3:
                        IntentLoader.loadSeeOtherApps(MainActivity.this);
                        break;
                }
            }
        });
    }

    private void placeFragment(String fragmentTag){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        int placeHolder = R.id.fragment_main;
        switch (fragmentTag){
            case TAG_FRAG_COUNTER:
                transaction.replace(placeHolder, new CounterFragment(), TAG_FRAG_COUNTER);
                break;
            case TAG_FRAG_CONVERTER:
                transaction.replace(placeHolder, new ConverterFragment(), TAG_FRAG_CONVERTER);
                break;
        }
        transaction.commit();
        manager.executePendingTransactions();
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onFragmentStart(int titleRes) {
        toolbarTitle.setText(titleRes);
    }
}
