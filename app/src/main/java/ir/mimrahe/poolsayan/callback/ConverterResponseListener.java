package ir.mimrahe.poolsayan.callback;

import java.util.ArrayList;

import ir.mimrahe.poolsayan.model.ConverterHistoryModel;


public interface ConverterResponseListener {
    void onResponse(ArrayList<ConverterHistoryModel> responseItems);
}
