package ir.mimrahe.poolsayan.callback;

import java.util.ArrayList;

import ir.mimrahe.poolsayan.model.CounterHistoryModel;

public interface CounterResponseListener {
    void onResponse(ArrayList<CounterHistoryModel> responseItems);
}
