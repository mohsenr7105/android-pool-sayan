package ir.mimrahe.poolsayan.model;

public class NavItem {
    private int mLogoRes, mTitleRes;

    public NavItem(int titleRes, int logoRes){
        mLogoRes = logoRes;
        mTitleRes = titleRes;
    }

    public int getLogoRes(){
        return mLogoRes;
    }

    public int getTitleRes(){
        return mTitleRes;
    }
}
