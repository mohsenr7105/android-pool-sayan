package ir.mimrahe.poolsayan.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ir.mimrahe.poolsayan.tool.DatabaseHelper;
import ir.mimrahe.poolsayan.tool.DateHelper;
import ir.mimrahe.poolsayan.tool.SimpleQueryBuilder;

public class ConverterHistoryModel extends DatabaseHelper {
    private Integer id;
    private String resultNumbersRial, resultNumbersTooman;
    private String resultInWordsRial, resultInWordsTooman;
    private String title;
    private Date createdAt;

    private enum Columns {
        ID("_id"), ResultNumbersRial("result_numbers_rial"), ResultNumbersTooman("result_numbers_tooman"),
        ResultInWordsRial("result_inwords_rial"), ResultInWordsTooman("result_inwords_tooman"),
        Title("title"), CreatedAt("created_at");

        private String colName;

        Columns(String colName) {
            this.colName = colName;
        }

        public String getColName() {
            return colName;
        }
    }

    public ConverterHistoryModel(Context context) {
        super(context);
    }

    public ConverterHistoryModel(Context context, String resultNumbersRial, String resultNumbersTooman, String resultInWordsRial, String resultInWordsTooman, String title, Date createdAt){
        super(context);
        this.resultNumbersRial = resultNumbersRial;
        this.resultNumbersTooman = resultNumbersTooman;
        this.resultInWordsRial = resultInWordsRial;
        this.resultInWordsTooman = resultInWordsTooman;
        this.title = title;
        this.createdAt = createdAt;
    }

    public ConverterHistoryModel(Context context, Integer id, String resultNumbersRial, String resultNumbersTooman, String resultInWordsRial, String resultInWordsTooman, String title, Date createdAt){
        super(context);
        this.id = id;
        this.resultNumbersRial = resultNumbersRial;
        this.resultNumbersTooman = resultNumbersTooman;
        this.resultInWordsRial = resultInWordsRial;
        this.resultInWordsTooman = resultInWordsTooman;
        this.title = title;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public String getResultNumbersRial(){
        return resultNumbersRial;
    }

    public String getResultNumbersTooman(){
        return resultNumbersTooman;
    }

    public String getResultInWordsRial(){
        return resultInWordsRial;
    }

    public String getResultInWordsTooman(){
        return resultInWordsTooman;
    }

    public String getTitle(){
        return title;
    }

    public Date getCreatedAt(){
        return createdAt;
    }

    public ConverterHistoryModel setId(Integer id) {
        this.id = id;
        return this;
    }

    public ConverterHistoryModel setResultNumbersRial(String resultNumbersRial){
        this.resultNumbersRial = resultNumbersRial;
        return this;
    }

    public ConverterHistoryModel setResultNumbersTooman(String resultNumbersTooman){
        this.resultNumbersTooman = resultNumbersTooman;
        return this;
    }

    public ConverterHistoryModel setResultInWordsRial(String resultInWordsRial){
        this.resultInWordsRial = resultInWordsRial;
        return this;
    }

    public ConverterHistoryModel setResultInWordsTooman(String resultInWordsTooman){
        this.resultInWordsTooman = resultInWordsTooman;
        return this;
    }

    public ConverterHistoryModel setTitle(String title){
        this.title = title;
        return this;
    }

    public ConverterHistoryModel setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
        return this;
    }

    private String getTableName() {
        return "converter_history";
    }

    private String getPKName() {
        return Columns.ID.getColName();
    }

    private String getPKValue() {
        return getId().toString();
    }

    public ConverterHistoryModel copy(Context context) {
        return new ConverterHistoryModel(context, getResultNumbersRial(), getResultNumbersTooman(), getResultInWordsRial(), getResultInWordsTooman(), getTitle(), getCreatedAt());
    }

    public Boolean save(){
        ContentValues values = new ContentValues();


        values.put(Columns.ResultNumbersRial.colName, getResultNumbersRial());
        values.put(Columns.ResultNumbersTooman.colName, getResultNumbersTooman());
        values.put(Columns.ResultInWordsRial.colName, getResultInWordsRial());
        values.put(Columns.ResultInWordsTooman.colName, getResultInWordsTooman());
        values.put(Columns.Title.colName, getTitle());
        values.put(Columns.CreatedAt.colName, DateHelper.dateToString(getCreatedAt()));

        long _id = getDatabase().insert(getTableName(), null, values);
        close();
        if(_id == -1)
            return false;

        id = (int) _id;
        return true;
    }

    public static ArrayList<ConverterHistoryModel> findAll(Context context){
        ConverterHistoryModel searcher = new ConverterHistoryModel(context);
        Cursor found = searcher.find(new SimpleQueryBuilder().setTableName(searcher.getTableName())
                .setOrderBy(Columns.ID.getColName(), true));
        try {
            return pushInList(context, found);
        } finally {
            searcher.close();
        }
    }

    public static ArrayList<ConverterHistoryModel> findWithLimit(Context context, int skip, int count){
        ConverterHistoryModel searcher = new ConverterHistoryModel(context);
        Cursor found = searcher.find(new SimpleQueryBuilder().setTableName(searcher.getTableName())
        .setOrderBy(Columns.CreatedAt.getColName(), true)
        .setLimit(skip, count));

        try {
            return pushInList(context, found);
        } finally {
            searcher.close();
        }
    }

    private static ArrayList<ConverterHistoryModel> pushInList(Context context, Cursor found){
        if (found == null)
            return null;

        ArrayList<ConverterHistoryModel> list = new ArrayList<>();
        if (found.moveToFirst()){

            do{
                ConverterHistoryModel converterHistory = new ConverterHistoryModel(context);
                converterHistory.setId(found.getInt(found.getColumnIndexOrThrow(Columns.ID.getColName())))
                        .setTitle(found.getString(found.getColumnIndexOrThrow(Columns.Title.getColName())))
                        .setResultNumbersRial(found.getString(found.getColumnIndexOrThrow(Columns.ResultNumbersRial.getColName())))
                        .setResultNumbersTooman(found.getString(found.getColumnIndexOrThrow(Columns.ResultNumbersTooman.getColName())))
                        .setResultInWordsRial(found.getString(found.getColumnIndexOrThrow(Columns.ResultInWordsRial.getColName())))
                        .setResultInWordsTooman(found.getString(found.getColumnIndexOrThrow(Columns.ResultInWordsTooman.getColName())))
                        .setCreatedAt(DateHelper.stringToDate(found.getString(found.getColumnIndexOrThrow(Columns.CreatedAt.getColName()))));

                list.add(converterHistory);
            } while (found.moveToNext());
        }
        found.close();

        return list;
    }

    public void deleteWithID(){
        SimpleQueryBuilder qb = new SimpleQueryBuilder().
                setTableName(getTableName())
                .setWhere(getPKName())
                .setWhereArgs(getPKValue());
        delete(qb);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "Converter History #%d, numbers rial: %s, in words rial: %s, numbers tooman: %s, in words tooman: %s, title: %s, created at: %s",
                getId(), getResultNumbersRial(), getResultInWordsRial(), getResultNumbersTooman(), getResultInWordsTooman(), getTitle(), getCreatedAt());
    }
}
