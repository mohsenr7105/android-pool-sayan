package ir.mimrahe.poolsayan.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ir.mimrahe.poolsayan.tool.DatabaseHelper;
import ir.mimrahe.poolsayan.tool.DateHelper;
import ir.mimrahe.poolsayan.tool.SimpleQueryBuilder;

public class CounterHistoryModel extends DatabaseHelper {
    private Integer id;
    private String currency;
    private String resultInWords;
    private String resultNumbers;
    private JSONObject items;
    private String title;
    private Date createdAt;

    private enum Columns{
        ID("_id"), Currency("currency"), ResultInWords("result_inwords"), ResultNumbers("result_numbers")
        , Items("items"), Title("title"), CreatedAt("created_at");

        private String colName;

        Columns(String colName){
            this.colName = colName;
        }

        public String getColName(){
            return colName;
        }
    }

    public CounterHistoryModel(Context context) {
        super(context);
    }

    public CounterHistoryModel(Context context, String currency, String resultNumbers, String resultInWords, JSONObject items, String title, Date createdAt){
        super(context);
        this.currency = currency;
        this.resultNumbers = resultNumbers;
        this.resultInWords = resultInWords;
        this.items = items;
        this.title = title;
        this.createdAt = createdAt;
    }

    public CounterHistoryModel(Context context, Integer id, String currency, String resultNumbers, String resultInWords, JSONObject  items, String title, Date createdAt){
        super(context);
        this.id = id;
        this.currency = currency;
        this.resultNumbers = resultNumbers;
        this.resultInWords = resultInWords;
        this.items = items;
        this.title = title;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public String getCurrency() {
        return currency;
    }

    public String getResultNumbers() {
        return resultNumbers;
    }

    public String getResultInWords() {
        return resultInWords;
    }

    public JSONObject getItems() {
        return items;
    }

    public String getTitle(){
        return title;
    }

    public Date getCreatedAt(){
        return createdAt;
    }

    public CounterHistoryModel setId(Integer id) {
        this.id = id;
        return this;
    }

    public CounterHistoryModel setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public CounterHistoryModel setResultNumbers(String resultNumbers) {
        this.resultNumbers = resultNumbers;
        return this;
    }

    public CounterHistoryModel setResultInWords(String resultInWords) {
        this.resultInWords = resultInWords;
        return this;
    }

    public CounterHistoryModel setItems(JSONObject items) {
        this.items = items;
        return this;
    }

    public CounterHistoryModel setTitle(String title){
        this.title = title;
        return this;
    }

    public CounterHistoryModel setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
        return this;
    }

    private String getTableName() {
        return "counter_history";
    }

    private String getPKName() {
        return Columns.ID.getColName();
    }

    private String getPKValue() {
        return getId().toString();
    }

    public CounterHistoryModel copy(Context context) {
        return new CounterHistoryModel(context, getCurrency(), getResultNumbers(), getResultInWords(), getItems(), getTitle(), getCreatedAt());
    }

    public Boolean save(){
        ContentValues values = new ContentValues();

        values.put(Columns.Currency.colName, getCurrency());
        values.put(Columns.ResultNumbers.colName, getResultNumbers());
        values.put(Columns.ResultInWords.colName, getResultInWords());
        values.put(Columns.Items.colName, getItems().toString());
        values.put(Columns.Title.colName, getTitle());
        values.put(Columns.CreatedAt.colName, DateHelper.dateToString(getCreatedAt()));

        long _id = getDatabase().insert(getTableName(), null, values);
        close();
        if(_id == -1)
            return false;

        id = (int) _id;
        return true;
    }

    public static ArrayList<CounterHistoryModel> findAll(Context context){
        CounterHistoryModel searcher = new CounterHistoryModel(context);
        Cursor found = searcher.find(new SimpleQueryBuilder().setTableName(searcher.getTableName())
                .setOrderBy(Columns.ID.getColName(), true));
        try {
            return pushInList(context, found);
        } finally {
            searcher.close();
        }
    }

    public static ArrayList<CounterHistoryModel> findWithLimit(Context context, int skip, int count){
        CounterHistoryModel searcher = new CounterHistoryModel(context);
        Cursor found = searcher.find(new SimpleQueryBuilder().setTableName(searcher.getTableName())
        .setOrderBy(Columns.ID.getColName(), true)
        .setLimit(skip, count));

        return pushInList(context, found);
    }

    private static ArrayList<CounterHistoryModel> pushInList(Context context, Cursor found){
        if (found == null)
            return null;

        ArrayList<CounterHistoryModel> list = new ArrayList<>();
        if (found.moveToFirst()){

            do{
                CounterHistoryModel counterHistory = new CounterHistoryModel(context);
                counterHistory.setId(found.getInt(found.getColumnIndexOrThrow(Columns.ID.getColName())))
                        .setTitle(found.getString(found.getColumnIndexOrThrow(Columns.Title.getColName())))
                        .setCurrency(found.getString(found.getColumnIndexOrThrow(Columns.Currency.getColName())))
                        .setResultNumbers(found.getString(found.getColumnIndexOrThrow(Columns.ResultNumbers.getColName())))
                        .setResultInWords(found.getString(found.getColumnIndexOrThrow(Columns.ResultInWords.getColName())))
                        .setCreatedAt(DateHelper.stringToDate(found.getString(found.getColumnIndexOrThrow(Columns.CreatedAt.getColName()))));
                try {
                    counterHistory.setItems(new JSONObject(found.getString(found.getColumnIndexOrThrow(Columns.Items.getColName()))));
                } catch (JSONException e){
                    counterHistory.setItems(null);
                }
                list.add(counterHistory);

            } while (found.moveToNext());
        }
        found.close();

        return list;
    }

    public void deleteWithID(){
        SimpleQueryBuilder qb = new SimpleQueryBuilder().
                setTableName(getTableName())
                .setWhere(getPKName())
                .setWhereArgs(getPKValue());
        delete(qb);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "Counter History #%d ,currency: %s,result numbers: %s,result in words: %s,items: %s,title: %s,created at: %s",
                getId(), getCurrency(), getResultNumbers(), getResultInWords(), getItems(), getTitle(), getCreatedAt());
    }
}
