package ir.mimrahe.poolsayan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.model.ConverterHistoryModel;
import ir.mimrahe.poolsayan.model.CounterHistoryModel;
import ir.mimrahe.poolsayan.tool.DateHelper;
import ir.mimrahe.poolsayan.tool.MiniHelpers;
import ir.mimrahe.poolsayan.variable.Currency;

public class ConverterHistoryAdapter extends ArrayAdapter<ConverterHistoryModel> {
    private static class ViewHolder{
        TextViewWithFont title, date, number, inWords;
    }

    public ConverterHistoryAdapter(@NonNull Context context, @NonNull List<ConverterHistoryModel> objects) {
        super(context, -1, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ConverterHistoryModel converterHistory = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.layout_converter_history_item, parent, false);
            viewHolder.title = (TextViewWithFont) convertView.findViewById(R.id.textViewHistoryTitle);
            viewHolder.date = (TextViewWithFont) convertView.findViewById(R.id.textViewHistoryDate);
            viewHolder.number = (TextViewWithFont) convertView.findViewById(R.id.textViewHistoryNumber);
            viewHolder.inWords = (TextViewWithFont) convertView.findViewById(R.id.textViewHistoryInWords);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.title.setText(converterHistory.getTitle());
        viewHolder.date.setText(DateHelper.g2Jalali(converterHistory.getCreatedAt()));
        viewHolder.number.setText(String.format("%s %s", converterHistory.getResultNumbersTooman(), getContext().getString(R.string.tooman)));
        viewHolder.inWords.setText(converterHistory.getResultInWordsTooman());

        return convertView;
    }
}
