package ir.mimrahe.poolsayan.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.model.NavItem;

public class NavDrawerAdapter extends ArrayAdapter<NavItem> {
    private static class ViewHolder{
        ImageView logo;
        TextViewWithFont title;
    }
    private int mRes;

    public NavDrawerAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull NavItem[] objects) {
        super(context, resource, objects);
        mRes = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        NavItem navItem = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(mRes, parent, false);
            viewHolder.logo = (ImageView) convertView.findViewById(R.id.imageViewIcon);
            viewHolder.title = (TextViewWithFont) convertView.findViewById(R.id.textViewTitle);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.logo.setImageResource(navItem.getLogoRes());
        viewHolder.title.setText(navItem.getTitleRes());

        return convertView;
    }
}
