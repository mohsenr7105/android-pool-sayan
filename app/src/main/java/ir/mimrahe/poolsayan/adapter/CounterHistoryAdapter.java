package ir.mimrahe.poolsayan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.model.CounterHistoryModel;
import ir.mimrahe.poolsayan.tool.DateHelper;
import ir.mimrahe.poolsayan.tool.MiniHelpers;
import ir.mimrahe.poolsayan.variable.Currency;

public class CounterHistoryAdapter extends ArrayAdapter<CounterHistoryModel> {
    private static class ViewHolder{
        TextViewWithFont title;
        TextViewWithFont date;
        TextViewWithFont numberAndCount;
    }

    public CounterHistoryAdapter(@NonNull Context context, @NonNull List<CounterHistoryModel> objects) {
        super(context, -1, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CounterHistoryModel counterHistory = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.layout_counter_history_item, parent, false);
            viewHolder.title = (TextViewWithFont) convertView.findViewById(R.id.textViewHistoryTitle);
            viewHolder.date = (TextViewWithFont) convertView.findViewById(R.id.textViewHistoryDate);
            viewHolder.numberAndCount = (TextViewWithFont) convertView.findViewById(R.id.textViewHistoryNumberAndCount);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.title.setText(counterHistory.getTitle());
        viewHolder.date.setText(DateHelper.g2Jalali(counterHistory.getCreatedAt()));
        viewHolder.numberAndCount.setText(joinNumberAndCount(counterHistory));

        return convertView;
    }

    private Spanned joinNumberAndCount(CounterHistoryModel counterHistory){
        String number = String.format(
                "<b>%s %s = </b>",
                counterHistory.getResultNumbers(),
                getContext().getString((counterHistory.getCurrency().equals(Currency.Tooman.getValue()) ? R.string.tooman : R.string.rial))
        );
        ArrayList<String> count = new ArrayList<>();
        Iterator<String> iter = counterHistory.getItems().keys();
        while (iter.hasNext()) {
            String key = iter.next();
            Integer value = 0;
            try {
                value = (Integer) counterHistory.getItems().get(key);
            } catch (JSONException e) {
                // Something went wrong!
            }
            count.add(String.format(
                    Locale.ENGLISH,
                    "(%d x %s)",
                    value,
                    MiniHelpers.numberFormat(Integer.valueOf(key) * (counterHistory.getCurrency().equals(Currency.Tooman.getValue()) ? 1 : 10))
                    )
            );
        }
        String countJoined = String.format("<small>%s</samll>", TextUtils.join(" + " , count));

        return Html.fromHtml(number + countJoined);
    }
}
