package ir.mimrahe.poolsayan.tool;

public class SimpleQueryBuilder {
    private String mTableName, mSelection, mWhere, mGroupBy, mHaving, mOrderBy, mLimit;
    private String[] mSelectionArgs, mWhereArgs;

    public SimpleQueryBuilder setTableName(String tableName){
        mTableName = tableName;
        return this;
    }

    public String getTableName(){
        return mTableName;
    }

    public SimpleQueryBuilder setSelection(String selection){
        mSelection = selection;
        return this;
    }

    public String getSelection(){
        return mSelection;
    }

    public SimpleQueryBuilder setWhere(String where){
        mWhere = where + " = ?";
        return this;
    }

    public String getWhere(){
        return mWhere;
    }

    public SimpleQueryBuilder setSelectionArgs(String selectionArgs){
        mSelectionArgs = new String[]{selectionArgs};
        return this;
    }

    public String[] getSelectionArgs(){
        return mSelectionArgs;
    }

    public SimpleQueryBuilder setWhereArgs(String whereArgs){
        mWhereArgs = new String[]{whereArgs};
        return this;
    }

    public String[] getWhereArgs(){
        return mWhereArgs;
    }

    public SimpleQueryBuilder setGroupBy(String groupBy){
        mGroupBy = groupBy;
        return this;
    }

    public String getGroupBy(){
        return mGroupBy;
    }

    public SimpleQueryBuilder setHaving(String having){
        mHaving = having;
        return this;
    }

    public String getHaving(){
        return mHaving;
    }

    public SimpleQueryBuilder setOrderBy(String orderBy, Boolean desc){
        mOrderBy = orderBy + " " + (desc ? "DESC" : "ASC");
        return this;
    }

    public String getOrderBy(){
        return mOrderBy;
    }

    public SimpleQueryBuilder setLimit(Integer skip, Integer count){
        mLimit = skip + ", " + count;
        return this;
    }

    public String getLimit(){
        return mLimit;
    }
}
