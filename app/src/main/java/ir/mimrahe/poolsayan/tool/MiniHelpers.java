package ir.mimrahe.poolsayan.tool;

import java.text.NumberFormat;

public class MiniHelpers {
    public static String numberFormat(long number) {
        return NumberFormat.getNumberInstance().format(number);
    }
}
