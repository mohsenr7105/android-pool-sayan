package ir.mimrahe.poolsayan.tool;

import android.content.Context;
import android.content.SharedPreferences;

import ir.mimrahe.poolsayan.variable.Currency;

public class PrefManager {
    private static SharedPreferences mSharedPreferences;
    private static final String PREF_NAME = "settings";
    private static final String KEY_CURRENCY = "currency";
    private static final String KEY_SHOW_ADDITIONAL_BILLS = "show_additional_bills";

    public static void init(Context context){
        if (mSharedPreferences == null){
            mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }
    }

    private static SharedPreferences.Editor getEditor(){
        return mSharedPreferences.edit();
    }

    public static void destroy(){
        mSharedPreferences = null;
    }

    public static boolean has(String key){
        return mSharedPreferences.contains(key);
    }

    public static void clear(){
        getEditor().clear();
        getEditor().apply();
    }

    public static String setCurrency(String value){
        getEditor().putString(KEY_CURRENCY, value).apply();
        return value;
    }

    public static String getCurrency(){
        return mSharedPreferences.getString(KEY_CURRENCY, Currency.Tooman.getValue());
    }

    public static boolean setShowAdditionalBills(boolean value){
        getEditor().putBoolean(KEY_SHOW_ADDITIONAL_BILLS, value).apply();
        return value;
    }

    public static boolean getShowAdditionalBills(){
        return mSharedPreferences.getBoolean(KEY_SHOW_ADDITIONAL_BILLS, true);
    }
}
