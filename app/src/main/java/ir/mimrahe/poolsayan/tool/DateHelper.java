package ir.mimrahe.poolsayan.tool;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {
    private static String simplePattern = "yyyy-MM-dd";
    private static SimpleDateFormat sdf = new SimpleDateFormat(simplePattern);

    public static String dateToString(Date date){
        return sdf.format(date);
    }

    public static Date stringToDate(String date){
        try {
            return sdf.parse(date);
        } catch (ParseException e){
            return null;
        }
    }

    public static String g2Jalali(Date gDate){
        Calendar gCalendar = Calendar.getInstance();
        gCalendar.setTime(gDate);

        int gregYear = gCalendar.get(Calendar.YEAR);
        int gregMonth = gCalendar.get(Calendar.MONTH) + 1;
        int gregDay = gCalendar.get(Calendar.DAY_OF_MONTH);

        int jalaliYear, jalaliMonth, jalaliDay, gregYear2;

        int[] array = {0,31,59,90,120,151,181,212,243,273,304,334};

        jalaliYear = gregYear <= 1600 ? 0 : 979;
        gregYear -= gregYear <= 1600 ? 621 : 1600;
        gregYear2 = gregMonth > 2 ? gregYear + 1 : gregYear;
        int days = (365 * gregYear) +
                ((gregYear2 + 3) / 4) -
                ((gregYear2 + 99) / 100) +
                ((gregYear2 + 399) / 400) - 80 +
                gregDay + array[gregMonth - 1];
        jalaliYear += 33 * (days / 12053);
        days %= 12053;
        jalaliYear += 4 * (days / 1461);
        days %= 1461;
        jalaliYear += (days - 1) / 365;
        if (days > 365){
            days = (days - 1) % 365;
        }
        jalaliMonth = days < 186 ? 1 + (days / 31) : 7 + ((days - 186) / 30);
        jalaliDay = 1 + (days < 186 ? days % 31 : (days - 186) % 30);

        return String.format("%s/%s/%s", jalaliYear, jalaliMonth, jalaliDay);
    }
}
