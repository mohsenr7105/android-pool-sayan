package ir.mimrahe.poolsayan.tool;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import ir.mimrahe.poolsayan.BuildConfig;
import ir.mimrahe.poolsayan.R;
import ir.mimrahe.poolsayan.custom_component.EditTextWithFont;
import ir.mimrahe.poolsayan.custom_component.TextViewWithFont;
import ir.mimrahe.poolsayan.model.ConverterHistoryModel;
import ir.mimrahe.poolsayan.model.CounterHistoryModel;
import ir.mimrahe.poolsayan.variable.Currency;

public class Alerts {
    public static void showChangeCurrency(final Context context, final CounterAlertCallback alertCallback){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.select_currency);
        builder.setItems(R.array.currency_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String currency = which == 0 ? Currency.Rial.getValue() : Currency.Tooman.getValue();
                alertCallback.onCurrencyChanged(currency);
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void showSettings(final Context context, final CounterAlertCallback alertCallback){
//        final ArrayList selectedItems = new ArrayList();  // Where we track the selected items
        final boolean[] checkedItems = new boolean[1];
        checkedItems[0] = PrefManager.getShowAdditionalBills();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Set the dialog title
        builder.setTitle(R.string.settings)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setMultiChoiceItems(R.array.settings, checkedItems,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                checkedItems[which] = isChecked;
                            }
                        })
                // Set the action buttons
                .setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK, so save the mSelectedItems results somewhere
                        // or return them to the component that opened the dialog
                        alertCallback.onSettingsChanged(checkedItems);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void showSave(final Context context, final AlertCallback alertCallback){
        LayoutInflater li = LayoutInflater.from(context);
        View dialogView = li.inflate(R.layout.dialog_save, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(dialogView);

        final EditTextWithFont titleInput = (EditTextWithFont) dialogView.findViewById(R.id.editTextTitle);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                alertCallback.onSaveInHistory(titleInput.getText().toString());
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public static void showCounterHistoryDetails(final Context context, CounterHistoryModel counterHistory){
        LayoutInflater li = LayoutInflater.from(context);
        View dialogView = li.inflate(R.layout.dialog_counter_history_details, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(dialogView);

        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryDate))
                .setText(DateHelper.g2Jalali(counterHistory.getCreatedAt()));
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryTitle))
                .setText(counterHistory.getTitle());
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryNumber))
                .setText(Html.fromHtml(String.format(
                        "<b>%s %s</b>",
                        counterHistory.getResultNumbers(),
                        context.getString((counterHistory.getCurrency().equals(Currency.Tooman.getValue()) ? R.string.tooman : R.string.rial))
                )));
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryInWords))
                .setText(counterHistory.getResultInWords());

        ArrayList<String> count = new ArrayList<>();
        Iterator<String> iter = counterHistory.getItems().keys();
        while (iter.hasNext()) {
            String key = iter.next();
            Integer value = 0;
            try {
                value = (Integer) counterHistory.getItems().get(key);
            } catch (JSONException e) {}

            int multiply = counterHistory.getCurrency().equals(Currency.Tooman.getValue()) ? 1 : 10;

            count.add(String.format(
                    Locale.ENGLISH,
                    "%d x %s = %d",
                    value,
                    numberFormat(Integer.valueOf(key) * multiply),
                    value * Integer.valueOf(key) * multiply
                    )
            );
        }
        String countJoined = String.format("<small>%s</samll>", TextUtils.join("<br>" , count));

        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryItems))
                .setText(Html.fromHtml(countJoined));

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton(R.string.ok, null);
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public static void showConverterHistoryDetails(final Context context, ConverterHistoryModel converterHistory){
        LayoutInflater li = LayoutInflater.from(context);
        View dialogView = li.inflate(R.layout.dialog_converter_history_details, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(dialogView);

        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryDate))
                .setText(DateHelper.g2Jalali(converterHistory.getCreatedAt()));
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryTitle))
                .setText(converterHistory.getTitle());
        String tooman = context.getString(R.string.tooman);
        String rial = context.getString(R.string.rial);
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryNumberTooman))
                .setText(String.format("%s %s", converterHistory.getResultNumbersTooman(), tooman));
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryNumberRial))
                .setText(String.format("%s %s", converterHistory.getResultNumbersRial(), rial));
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryInWordsTooman))
                .setText(String.format("%s %s", converterHistory.getResultInWordsTooman(), tooman));
        ((TextViewWithFont) dialogView.findViewById(R.id.textViewHistoryInWordsRial))
                .setText(String.format("%s %s", converterHistory.getResultInWordsRial(), rial));

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton(R.string.ok, null);
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private static String numberFormat(long number) {
        return NumberFormat.getNumberInstance().format(number);
    }

    public static void toast(Context context, int resId){
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
    }

    public static void toast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public interface CounterAlertCallback{
        void onCurrencyChanged(String currency);
        void onSettingsChanged(boolean[] settings);
    }

    public interface AlertCallback{
        void onSaveInHistory(String title);
    }
}
