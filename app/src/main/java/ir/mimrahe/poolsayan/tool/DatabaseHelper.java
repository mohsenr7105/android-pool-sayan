package ir.mimrahe.poolsayan.tool;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ir.mimrahe.poolsayan.BuildConfig;

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context mContext;
    private int mDbVersion;

    public DatabaseHelper(Context context) {
        super(context, BuildConfig.DB_NAME, null, BuildConfig.DB_VERSION);
        mContext = context;
        mDbVersion = BuildConfig.DB_VERSION;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        modifyDatabase(db, 1, mDbVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        modifyDatabase(db, oldVersion + 1, newVersion);
    }

    /**
     * reads database version files and modifies database
     * @param db database instance
     * @param oldVersion older version of database
     * @param newVersion newer version of database
     */
    private void modifyDatabase(SQLiteDatabase db, int oldVersion, int newVersion){
        for (int i = oldVersion; i <= newVersion; i++) {
            for (String modification : compileScheme(i)) {
                db.execSQL(modification);
                Log.e("database modify", modification);
            }
        }
    }

    protected Context getContext() {
        return mContext;
    }

    public SQLiteDatabase getDatabase() {
            return getWritableDatabase();
    }

    @Override
    public synchronized void close() {
        super.close();
    }

    /**
     * reads database version file
     * @param databaseVersion version of database
     * @return array of database version file lines
     */
    private ArrayList<String> compileScheme(int databaseVersion) {
        ArrayList<String> modifications = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(getContext().getAssets().open("database/" + databaseVersion + ".sql")));

            String line = reader.readLine();

            while (line != null) {
                modifications.add(line);
                line = reader.readLine();
            }

        } catch (IOException e) {
            Log.e("scheme compile", "error while reading sql file version " + databaseVersion);
        }

        return modifications;
    }

    protected Cursor find(SimpleQueryBuilder qb){
            return getDatabase().query(
                    qb.getTableName(),
                    null,
                    qb.getSelection(),
                    qb.getSelectionArgs(),
                    qb.getGroupBy(),
                    qb.getHaving(),
                    qb.getOrderBy(),
                    qb.getLimit()
            );
    }

    protected void delete(SimpleQueryBuilder qb) {
        getDatabase().delete(qb.getTableName(), qb.getWhere(),qb.getWhereArgs());
    }

    /**
     * checks if new value is dirty for update
     * @param newValue new value of field
     * @param oldValue old value of field
     */
    public boolean isDirty(Object newValue, Object oldValue){
        return oldValue != null && !newValue.equals(oldValue);
    }
}
