package ir.mimrahe.poolsayan.tool;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;

import ir.mimrahe.poolsayan.R;

public class InWords {
    private static final InWords ourInstance = new InWords();

    private static InWords getInstance() {
        return ourInstance;
    }

    private InWords() {
    }

    public static String from(Context context, long number){
        // if number equals zero then return "zero"
        if (number == 0){
            return context.getResources().getString(R.string.n0);
        }
        // split number 3 digits by 3 into array list
        ArrayList<Long> splited = new ArrayList<>();
        do{
            splited.add(number % 1000);
            number /= 1000;
        } while(number != 0);
//        Log.e("splited", splited.toString());
        // get splited numbers in words and save into new array list
        ArrayList<String> collected = new ArrayList<>();
        for (long num: splited){
            collected.add(getInstance().getInWords(context, num));
        }
        // add postfix for numbers
        String[] names = context.getResources().getStringArray(R.array.names);
        for (int i = 0; i < collected.size(); i++){
            if (!collected.get(i).isEmpty()){
                collected.set(i, collected.get(i) + " " + names[i]);
            }
        }
        String result = "";
        if (!collected.isEmpty()){
            // remove empty items in collected
            while(collected.remove(""));
            // reverse collected for fix order
            Collections.reverse(collected);
            // join collected into string with delimiter "and"
            String delimiter = context.getString(R.string.inwords_3digits_delimiter);
            if(delimiter.isEmpty()){
                delimiter = " ";
            } else {
                delimiter = " " + delimiter + " ";
            }
            result = TextUtils.join(delimiter, collected);
        }
        return result;
    }

    private String getInWords(Context context, long number){
        if (number == 0){
            return "";
        }
        String[] array0_19strings = context.getResources().getStringArray(R.array.no0_19);
        String[] array20_90strings = context.getResources().getStringArray(R.array.no20_90);
        String[] array100_900strings = context.getResources().getStringArray(R.array.no100_900);
        ArrayList<String> result = new ArrayList<>();
//        Log.e("getInW: number=", String.valueOf(number));
        int mod100 = (int) number % 100;
        int mod10;
        if (mod100 > 9 && mod100 < 20){
//            Log.e("getInW: mod100=", String.valueOf(mod100));
            result.add(array0_19strings[mod100]);
            number /= 100;
        } else {
            mod10 = (int) number % 10;
//            Log.e("getInW: mod10 1", String.valueOf(mod10));
            if (mod10 != 0){
                result.add(array0_19strings[mod10]);
            }
            number /= 10;
            mod10 = (int) number % 10;
//            Log.e("getInW: mod10 2", String.valueOf(mod10));
            if(mod10 != 0){
                result.add(array20_90strings[mod10]);
            }
            number /= 10;
        }
        mod10 = (int) number % 10;
//        Log.e("getInW: mod10 3", String.valueOf(mod10));
        if (mod10 != 0){
            result.add(array100_900strings[mod10]);
        }
        if (!result.isEmpty()){
            Collections.reverse(result);
            String delimiter = context.getString(R.string.inwords_digits_delimiter);
            if(delimiter.isEmpty()){
                delimiter = " ";
            } else {
                delimiter = " " + delimiter + " ";
            }
            return TextUtils.join(delimiter, result);
        }
        return "";
    }
}
