package ir.mimrahe.poolsayan.tool;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import ir.mimrahe.poolsayan.BuildConfig;
import ir.mimrahe.poolsayan.R;

public class IntentLoader {
    public static void loadRateUs(Context context){
        try {
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setData(Uri.parse("bazaar://details?id=" + BuildConfig.APPLICATION_ID));
            intent.setPackage("com.farsitel.bazaar");
            context.startActivity(intent);
        } catch (Exception e){
            Alerts.toast(context, R.string.error_like);
        }
    }

    public static void loadShareApp(Context context){
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.app_sharing_message));
            sendIntent.setType("text/plain");
            context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.sharing_chooser_title)));
        } catch (Exception e){
            //
        }
    }

    public static void loadContactDev(Context context){
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("http://telegram.me/mohsen7105"));
            context.startActivity(Intent.createChooser(i, context.getString(R.string.message_contact)));
        } catch (Exception e){
            Alerts.toast(context, R.string.error_contact);
        }
    }

    public static void loadSeeOtherApps(Context context){
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("bazaar://collection?slug=by_author&aid=mimrahe"));
            intent.setPackage("com.farsitel.bazaar");
            context.startActivity(intent);
        } catch (Exception e){
            Alerts.toast(context, R.string.error_like);
        }
    }
}
